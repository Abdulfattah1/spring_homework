import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { UtilitiesModule } from '../utilities/utilities.module';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbSelectModule } from '@nebular/theme';

const IMPORT_MODULES = [
  RouterModule,
  TranslateModule,
  CommonModule,
  UtilitiesModule,
  NbSelectModule,
  HttpClientModule,
  ReactiveFormsModule,
  FormsModule,
]

const EXPORT_MODULES = [
  RouterModule,
  TranslateModule,
  CommonModule,
  UtilitiesModule,
  NbSelectModule,
  HttpClientModule,
  ReactiveFormsModule,
  FormsModule,
];

@NgModule({
  declarations: [],
  imports: [
    ...IMPORT_MODULES
  ],
  exports:[
    ...EXPORT_MODULES
  ]
})
export class SharedModule { }
