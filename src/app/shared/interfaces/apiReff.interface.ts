export interface apiReff {
    [tableName:string]:apiElement
}

export interface apiElement {
    data?:string,
    edit?:string,
    delete?:string,
    add?:string
}