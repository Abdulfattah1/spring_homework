import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthServiceService } from '../../auth/auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router:Router, 
    private _authService:AuthServiceService) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      //return true;
      let role_id = localStorage.getItem('role_id');
      let token = localStorage.getItem('token');
      if(token) {
        return true;
      }
      if(!token) {
        this.router.navigate(['auth','login']);
      }
      return false;     
       // if(!this._authService.isAuthenticated()) {
      //   this.router.navigate(['auth','login']);
      //   return false;
      // }

      return true;
  }
  
}
