import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PrivilegeService } from '../services/privilege.service';
import decode from 'jwt-decode';
@Injectable({
  providedIn: 'root'
})
export class PrivilegesGuard implements CanActivate {
  
  constructor(
    private router:Router,
    private _privilegesService:PrivilegeService
  ) {};
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const privileges = next.routeConfig.data;

      return true;
      let role_id = localStorage.getItem('role_id');
      if(!role_id) {
        this.router.navigate(['auth','login']);
        return false;
      }
      if(!privileges){
        return true;
      }
      let pass = false;
      privileges.foreach(item=>{
        if(item == role_id) {
          pass = true;
        }
      })
      if(!pass) {
        this.router.navigate(['auth','login']);
      }
      return pass;
  }
  
}
