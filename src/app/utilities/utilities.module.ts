import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../@theme/theme.module';
import { NbMenuModule, NbSpinnerModule, NbCardModule, NbLayoutModule, NbButtonModule, NbIconModule, NbToastrModule, NbTabsetModule, NbSelectModule, NbCheckboxModule, NbListModule } from '@nebular/theme';
import { TableComponent } from './components/table/table.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FormCreatorComponent } from './components/form-creator/form-creator.component';
import { NgSelectModule } from '@ng-select/ng-select';
// import {} from "@n"
// import {} from '@ngselect'
@NgModule({
  declarations: [
    TableComponent,
    FormCreatorComponent
  ],
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    ReactiveFormsModule,
    FormsModule,
    NgbModule,
    NbSpinnerModule,
    NbToastrModule.forRoot(),
    ThemeModule.forRoot(),
    NbLayoutModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbCardModule,
    NbTabsetModule,
    NbSelectModule,
    NbCheckboxModule,
    NgbModule,
    NbListModule,
    NbSelectModule,
    NgSelectModule
  ],
  exports:[
    TableComponent,
    ThemeModule,
    NbCardModule,
    NbLayoutModule,
    NbButtonModule,
    NbEvaIconsModule,
    NbIconModule,
    NbToastrModule,
    FormCreatorComponent,
    NbTabsetModule,
    NbSelectModule,
    NbCheckboxModule,
    NgbModule,
    NbListModule,
    NbSelectModule,
    NgSelectModule
  ]
})
export class UtilitiesModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: UtilitiesModule
    };
  }
}
