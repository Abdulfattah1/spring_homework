import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FacadeService } from './facade.service';
import { ApiService } from './api.service';
import decode from 'jwt-decode';



var i=0;

@Injectable({
  providedIn: 'root'
})
export class PrivilegeService {
  privileges={}
  public done:boolean = false;
  constructor(private _ApiService:ApiService,private router:Router) {
  }

  getMyPrivileges():Promise<any> {
    return new Promise((resolve,reject)=>{
      this._ApiService.get('privilege/my_privilege').toPromise()
      .then((data)=>{
        let Data:any;
        Data = data;
        Data.forEach((p)=>{
          this.privileges[p['pri_name']]=true
        })
        this.done = true;
        resolve(true);
      })
      .catch(err=>{
        this.router.navigate(['auth','login']);
        reject(false);
      })
    })
  }
  
  isAuthorized(privilege)
  {
    return this.checkAuth(privilege);
  }

  private checkAuth(privilege) {
    
    let token = localStorage.getItem('token');
    let role_id = localStorage.getItem('role_id');
    console.log(role_id);
    let pass = false;
    //if(token) {
    // let tokenPayload = decode(token);

      if(Array.isArray(privilege)){
        for(var i=0;i<privilege.length;i++)
        {
          if(role_id == privilege[i]) {
            pass = true;
          }
        }
      }
      return pass; 
    }
}
