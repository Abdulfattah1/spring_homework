import { Injectable } from '@angular/core';
import { apiReff } from '../../shared/interfaces/apiReff.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiReferenceService {

  apiRef:apiReff ;
  constructor() {
    this.apiRef = {
      users:{
        data:"users/",
        add:"users/add",
        edit:"users/edit",
        delete:"users/delete"
      }
    }
  }

}
