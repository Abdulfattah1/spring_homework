import { Injectable } from '@angular/core';
import { field} from '../components/form-creator/settings/formInterface';
import { Validators } from '@angular/forms';
import { PrivilegeService } from './privilege.service';
import { NbMenuItem } from '@nebular/theme';
import { TranslationService } from './translation.service';

interface Fields {
  [name:string]:field[];
}

@Injectable({
  providedIn: 'root'
})
export class FieldsService {

  private fields;
  constructor(
    private _privilegesService:PrivilegeService,
    private _translationService:TranslationService
  ) { 
    this.fields = {
      users:[
        {name:'email',title:'email',type:'email' ,placeholder:'enter your email',validators:[Validators.required]},
        {name:'firstName',title:'first name',type:'string' ,placeholder:'enter your first name',validators:[Validators.required]},
        {name:'lastName',title:'last name',type:'string' ,placeholder:'enter your last name',validators:[Validators.required]},
      ],
      privileges:[
        {name:'name',title:'name',type:'string' ,placeholder:'enter a name',validators:[Validators.required]} 
      ],
      roles:[
        {name:'name',title:'name',type:'string' ,placeholder:'enter a name',validators:[Validators.required]} 
      ],
      contacts:[
        {name:'first_name',title:'first Name',type:'string' ,placeholder:'enter a first name',validators:[Validators.required]},
        {name:'last_name',title:'last Name',type:'string' ,placeholder:'enter a last name',validators:[Validators.required]},
        {name:'mobile',title:'mobile',type:'number' ,placeholder:'enter a number',validators:[Validators.required]},
        {name:'address',title:'address',type:'string' ,placeholder:'enter an address',validators:[Validators.required]},
        {name:'version',title:'version',type:'number',hidden:true},
      ]
      // ,
      // register:[
      //   {name:'name',title:'name',type:'string' ,placeholder:'enter a name',validators:[Validators.required]},
      //   {name:'email',title:'email',type:'email' ,placeholder:'enter am email',validators:[Validators.required,Validators.email]},
      //   {name:'confirmPassword',title:'confirm Password',type:'string' ,placeholder:'re-enter the password',validators:[Validators.required]},
      //   {name:'confirmPassword',title:'confirm Password',type:'string' ,placeholder:'re-enter the password',validators:[Validators.required]},
      // ]
    };

    



  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.    
  }

  getMenuItems(list: NbMenuItem[]) {
    let out = [];
    for (var i = 0; i < list.length; i++) {
      if (
        !list[i].data
        || list[i].data.length == 0
        || this._privilegesService.isAuthorized(list[i]['data']['privilege'])) {
        list[i].title = this._translationService.translateUtterance(list[i].title);
        out.push(list[i]);
      }
      if (list[i].children) {
        list[i].children = this.getMenuItems(list[i].children);
      }
    }
    return out;
  }

  getFields(name:string):field[] {
    return this.fields[name];
  }
}
