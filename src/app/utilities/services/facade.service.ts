import { Injectable, Injector } from '@angular/core';
import { TranslationService } from './translation.service';
import { NbToastrService, NbMenuItem } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PrivilegeService } from './privilege.service';
import { TableService } from '../components/table/table.service';
import { ApiService } from './api.service';
import { HttpParams } from '@angular/common/http';
import { FormCreatorService } from '../components/form-creator/form-creator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FieldsService } from './fields.service';

@Injectable({
  providedIn: 'root'
})
export class FacadeService {


  constructor(
    private router:Router,
    private _NgbModal:NgbModal,
    private _ApiService:ApiService,
    private _tableService:TableService,
    private activatedRoute:ActivatedRoute,
    private _fieldsService:FieldsService,
    // private _NbToastrService:NbToastrService,
    private _PrivilegeService:PrivilegeService,
    private _formCreatorService:FormCreatorService,
    private _translationService:TranslationService
    ) { }

  //modal service

  // api service 

  public get(name:string, params?: HttpParams) {
    return this._ApiService.get(name,params);
  }

  public post(name:string,data:any) {
    return this._ApiService.post(name,data);
  }

  public put(name:string,params:string | number,data:any) {
    return this._ApiService.put(name,params,data);
  }

  public delete(name:string,id:number) {
    return this._ApiService.post(name,id);
  }




  //table service
  public updateTable() {
    return this._tableService.updateRow$;
  }
  public updateRow() {
    return this._tableService.updateRow$;
  }

  public selectedRow() {
    return this._tableService.selectedRow$;
  }

  public customActions() {
    return this._tableService.customActions$;
  } 

  public getData(url:string,pagination?:{page:number,count?:number},criteria?:string) {
    return this._tableService.getData(url,pagination,criteria);
  }


  //form service 
  public getFormData(url:string,params?:HttpParams) {
    return this._formCreatorService.getData(url,params)
  }

  public submitForm(url:string,values:{},type:string) {

  }

  //fields service

  public getFields(name:string) {
    return this._fieldsService.getFields(name);
  }

  public getMenuItems(MENU:NbMenuItem[]) {
    return this._fieldsService.getMenuItems(MENU);
  }



  public addRecord(url:string,values:{}) {
    return new Promise((resolve,reject)=>{
    this._formCreatorService.addRecord(url,values)
    .subscribe(data=>{
      // this.successToastr(data['msg']);
      // setTimeout(() => {
      //   this.router.navigate(['../'],{relativeTo:this.activatedRoute});
      // }, 1000);
      resolve(true);
    },err=>{
      reject(false);
      //this.dangerToastr(err['msg']);
    })
  })
  }

  public deleteRecord(url:string,params:string | number) {
    return this._tableService.deleteRecord(url,params);
  }

  public updateRecord(url:string,params:string | number,values:{}) {
    return new Promise((resolve,reject)=>{
    this._formCreatorService.updateRecord(url,params,values)
    .subscribe(data=>{
      resolve(true);
    },err=>{
      reject(false);
      // this.dangerToastr(err['msg']);
    })
  })
  }
  //toastr service

  // public successToastr(msg) {
  //   this._NbToastrService.success(msg,'success');
  // }
  
  // public dangerToastr(msg) {
  //   this._NbToastrService.danger(msg,'error');
  // }
  //privileges service

  public getPrivileges() {
    return this._PrivilegeService.privileges;
  }

  public getMyPrivielges() {
    return this._PrivilegeService.getMyPrivileges();
  }

  public isAuthorizedr(privileges:string[] | string) {
    return this._PrivilegeService.isAuthorized(privileges);
  }
    
  // translation service
  public translate(word:string) {
    return this._translationService.translateUtterance(word);
  }

  public toggleLang() {
    return this._translationService.toggleLang();
  }

  public checkLang() {
    return this._translationService.checkLang();
  }

  public getLangs() {
    return this._translationService.langs;
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
  }
}
