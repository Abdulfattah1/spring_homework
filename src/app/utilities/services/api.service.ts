import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn:'root'
})
export class ApiService {
  api = environment.api;
  header = new HttpHeaders();
  constructor(
    private http: HttpClient
  ) {

  }

  // Uses http.get() to load data from a single API endpoint
  get(name:string, params?: HttpParams) {
    if (localStorage.getItem('token')) {
      this.header = this.header.set('Authorization',"Bearer " + localStorage.getItem('token'));
    }
    console.log(this.header);
    if (params) {
      return this.http.get(this.api + name, { params: params, headers: this.header });
    }
    return this.http.get(this.api + name, { params: params, headers: this.header });

  }
  post(name:string, data:any) {
    if (localStorage.getItem('token'))
      this.header = this.header.set('Authorization',"Bearer " + localStorage.getItem('token'));
    return this.http.post(this.api + name, data, { headers: this.header });
  }
  put(name:string,params:string | number,data:any) {
    if (localStorage.getItem('token'))
      this.header = this.header.set('Authorization',"Bearer " + localStorage.getItem('token'));

    return this.http.put(this.api + name + '/' + params, data, { headers: this.header });
  }
  delete(name:string, id:string | number) {
    if (localStorage.getItem('token'))
      this.header = this.header.set('Authorization',"Bearer " + localStorage.getItem('token'));

    return this.http.delete<{ success: boolean, errorCode: number, message: string }>(this.api + name + '/' + id, { headers: this.header });
  }

}
