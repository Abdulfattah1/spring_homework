import { TestBed } from '@angular/core/testing';

import { ApiReferenceService } from './api-reference.service';

describe('ApiReferenceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiReferenceService = TestBed.get(ApiReferenceService);
    expect(service).toBeTruthy();
  });
});
