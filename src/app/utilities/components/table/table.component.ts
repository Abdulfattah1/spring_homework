import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { settings ,columns, action, defaultAction,Pagination, defaultActionsDisplay} from './settings';
import { Data } from './data';
import { FormGroup } from '@angular/forms';
import { TableService } from './table.service';
import { Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FacadeService } from '../../services/facade.service';
import { Router, ActivatedRoute } from '@angular/router';
// import { PrivilegeService } from '../../services/privilege/privilege.service';
// import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ConfirmComponent } from '../confirm/confirm.component';
// import { TranslationService } from '../../services/translation.service';
// import { ToastrService } from 'ngx-toastr';
// import { MyModalComponent } from '../my-modal/my-modal.component';
// import { JustificationModalComponent } from '../justification-modal/justification-modal.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit,OnChanges {
  
  
  @Input('data') data:any[] = [];
  @Input('sectionId') sectionId:number;
  @Input('settings') settings:settings;
  Data:any[];
  localData:Data;
  columns:columns;
  classes:string[];
  filters:FormGroup;
  columnsNames:string[];
  pagination:Pagination;
  columnsTitles:string[];
  loading:boolean = true;
  customActions:action[];
  defaultActions:defaultAction[];
  defaultActionsStatus:defaultActionsDisplay;
  columnsSettings:columns;
  selectsForms:FormGroup;
  displayPagination:boolean = false;
  mergesArray:any[];
  searchFields:{[propName:string]:string};
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private router:Router,
    private toastr:NbToastrService,
    private _facadeService:FacadeService,
    private _tableService:TableService,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {     
    console.log(this.settings);
    this.initTableSettings();

    this._facadeService.updateTable()
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.reload();
    })

    this._facadeService.updateRow()
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.updateRow(data);
    })
  }

  reload() {
    this.searchFields = {};
    this.filters.reset();
    this.getData();
  }

  updateRow(data:{index:number,data:{}}) {
    this.Data[data['index']] = data['data'];
  }
  
  initTableSettings() {
    this.mergesArray = [];
    let settings:settings = {...this.settings};
    this.localData = new Data(settings);
    //this.localData.setPrivileges(this._facadeService.getPrivileges());
    this.columns = this.localData.getColumns();
    this.pagination = this.localData.setPagination();
    this.classes = this.localData.getColumnsClasses();
    this.columnsNames = this.localData.getColumnsNames();
    this.columnsTitles = this.localData.getColumnsTitle();
    this.customActions = this.localData.getCustomActions();

    this.defaultActionsStatus = this.localData.getDefaultActionsStatus();

    if(this.localData.hasFilters()) {
      this.localData.createFilters();
      this.filters = this.localData.getFilters(); 
      this.filters.valueChanges
      .pipe(
        takeUntil(this.distroy$),
        debounceTime(1000),
        distinctUntilChanged(),
      ).subscribe(values=>{
        this.searchFields = this.localData.initFitlers(values);
        this.pageChange(1);
      })
    }

    this.getData();
  }
  getData() {
  
    if(this.localData.isEnternal()){
      this.localData.setData([...this.data]);
      this.Data = this.localData.getFilteredData();
      this.localData.createSelect();
      this.selectsForms = this.localData.getSelects();
      this.loading = false;
      this.displayPagination = true;
    } else if(this.localData.isExternal()) {
      this.getExternalData();
    }
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.getData();
  }

  merge(id:number) {
    let arr = [this.selectsForms.value];
    let _data = [];
    let _ids = [];
    Object.keys(this.selectsForms.value)
    .forEach(key=>{
      if(this.selectsForms.value[key]) {
        _data.push(this.Data[key]);
        _ids.push(key);
      }
    })
    this._tableService.customActions$
    .next({
      action:'merge',
      data:_data,
      index:this.sectionId,
      ids:_ids
    })
  }
  pageChange(page:number) {
    this.pagination.page = page;
    this.localData.updatePagination(page);
    if(this.localData.isEnternal()) {
      this.Data = this.localData.getFilteredData(this.searchFields);    
      this.pagination = this.localData.getPagination(); 
    } else if(this.localData.isExternal()){
      this.getExternalData();
    }
  }

  getExternalData() {
    let url = this.localData.getDataUrl();
    let queryParams = this.localData.initQueryParams(this.searchFields);
    if(this.settings.specialFilter)
        queryParams+=this.settings.specialFilter;
    
    let page = this.pagination.page || 1;
    let count = this.pagination.pageSize || 10;
    this._facadeService.getData(url,{page,count},queryParams)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.Data = data['data'];
      if(this.localData.isCustomGettingData()) {
        this.Data = this.localData.extractData(this.Data);
      }
      this.localData.setData(this.Data);
      this.localData.updatePagination(page,data['count']);
      this.pagination = this.localData.getPagination();
      this.loading = false;
      this.displayPagination = true;
    })
  }

  

  selectedRow(index:number) {
    let obj = {
      index,
      data:this.Data[index]
    }
    this._facadeService.selectedRow().next(obj);
  }

  actions(index:number,customAction:action) {
    let obj = {
      index,
      data:this.Data[index],
      action:customAction.name
    };
    if(customAction.type && customAction.type == 'modal') {
      customAction.attrs = customAction.attrs || [];
      // this.modal(customAction.title,customAction.attrs,this.Data[index])
      // .then(res=>{})
      // .catch(err=>{})
    }
    this._facadeService.customActions().next(obj);
  }

  deleteAction(index:number,deleteAction:defaultAction) {
    let obj = {
      index,
      data:this.Data[index],
      action:'delete'
    }
    // if(deleteAction.type == 'justification') {
    //   this.justufucationModal()
    //   .then(justification=>{
    //     let dataTosend = {
    //       [deleteAction.attrName]:this.Data[index][deleteAction.attrName],
    //       justification:justification.justification
    //     }
    //     this._tableService.delete(deleteAction.url,dataTosend)
    //     .pipe(
    //       tap(val=>this.loading = true),
    //       takeUntil(this.distroy$),
    //       tap(val=>this.loading = false)
    //     )
    //     .subscribe(data=>{
    //       this._toastrService.show
    //       this.Data = this.Data.splice(index,1);
    //   },err=>{
    //       this._toastrService.danger('error','error');
    //     })
    //   })
    //   .catch(err=>{})
    // }
  }

  add() {
    this.router.navigate(['add'],{relativeTo:this.activatedRoute})
  }

  edit(index:number) {
    let _id = this.Data[index]['id'];
    
    this.router.navigate(['edit',_id],{relativeTo:this.activatedRoute})
  }

  delete(index:number) {
    let _id = this.Data[index]['id'];
    let _url = this.settings.actions.default.delete.url;
    this._facadeService.deleteRecord(_url,_id)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.Data.splice(index,1);
      this.localData.setData(this.Data);
      this.toastr.success(data['message'],'success');
    },err=>{
      this.toastr.danger(err['message'],'error');
    })
  }



  // confirmModal():Promise<any> {
  //   let confirm = this._modalService.open(ConfirmComponent);
  //   confirm.componentInstance.title = this._translationService.translateUtterance('button.AreYouSure');
  //   return confirm.result;
  // }

  // justufucationModal():Promise<any> {
  //   let justification = this._modalService.open(JustificationModalComponent);
  //   justification.componentInstance.title = this._translationService.translateUtterance('Confirm.pleaseEnteraJustification');
  //   return justification.result;
  // }
  
  // modal(title:string,attrs:{name:string,title:string}[],row:{}) {

  //   let _attrs:{name:string,value:string | number}[] = [];
  //   let modal = this._modalService.open(MyModalComponent);

  //   for(var i = 0; i < attrs.length;i++) {
      
  //     let obj = {
  //       name:attrs[i].title,
  //       value:row[attrs[i].name]
  //     }
  //     _attrs.push(obj);
  //   }

  //   modal.componentInstance.title = title; 
  //   modal.componentInstance.data = _attrs;

  //   return modal.result;

  // }

  displayingModal() {

  }

  ngOnDestroy(): void {

    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();

  }

}
