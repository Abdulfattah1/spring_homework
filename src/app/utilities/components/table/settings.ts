

export interface Pagination {
    size:'lg' | 'sm',
    page:number,
    maxSize:number,
    pageSize:number,
    pageChange?:number,
    collectionSize:number
}

export interface settings {
    mode:"external" | "enternal",
    addUrl?:string,
    dataUrl?:string,
    deleteUrl?:string,
    params?:string,
    filters?:boolean,
    actions?:actions,
    columns:columns,
    specialFilter?:string,
    pagination?:Pagination,
    data:feachingData
}

export interface feachingData {
    url:string,
    custom?:{
        status:boolean,
        getData?(data:any):any
    }
}


export interface columns {
    classes?:any[],
    styles?:any[],
    elements?:elements
}

export interface elements {
    [propName:string]:column
}

export interface column  {
    classes?:string[],
    title:string,
    filterDisabled?:boolean,
    translateValue?:boolean,
    condition?:condition,
    type?:'string' | 'number'
}

export interface actions {
    custom?:action[],
    default?:defaultActions,
}

export interface defaultActions {
    add?:defaultAction,
    edit?:defaultAction,
    delete?:defaultAction,
    merge?:defaultAction
}

export interface defaultAction {
    url:string,
    params?:string,
    attrName:string,
    queryParams?:string,
    privileges?:string[],
    type?:'confirmation' | 'justification' | 'modal'
}

export interface action {
    privileges?:string[],
    name:string,
    title:string,
    icon?:string,
    attrs?:attr[],
    disabled?:{propName:string , value:number};
    type?:'confirmation' | 'justification' | 'modal'
}

export interface condition {
    status:boolean,
    false:string,
    true:string
}

export interface attr {
    name:string,
    title:string
}

export interface defaultActionsDisplay {
    add:boolean,
    view:boolean,
    edit:boolean,
    delete:boolean,
    export:boolean,
    merge:boolean
}


//example
// const test:settings = {
//     mode:"external",
//     actions:'ok',
//     filters:true,
//     columns:{
//       classes:['text-center'],
//       styles:[],
//       elements:{
//           abd:{
//               classes:[],
//               disabled:true,
//               title:'hh',
//               type:"number"
//           },
//           good:{
//             classes:[],
//             disabled:true,
//             title:'bb',
//             type:"number"
//         }
//       }
//     }
//   };