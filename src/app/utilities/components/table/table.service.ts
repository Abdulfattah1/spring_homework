import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ApiService } from '../../services/api.service';
import { FacadeService } from '../../services/facade.service';
// import { MainService } from '../main.service';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  updateTable$:Subject<boolean> = new Subject();
  updateRow$:Subject<{index:number,data:{}}> = new Subject();
  selectedRow$:Subject<{index:number,data:{}}> = new Subject();
  customActions$:Subject<{index:number,action:string,data:{},ids?:number[]}> = new Subject();
  pagerChanged$:Subject<number> = new Subject();
  constructor(
    private _ApiService:ApiService
  ) { }

  getData(url:string,pagination?:{page:number,count?:number},criteria?:string) {
    let fullUrl:string;
    fullUrl = url;
    if(pagination) {
      fullUrl+=`?start_page=${pagination.page}`;
      if(pagination.count) {
        fullUrl+=`&count=${pagination.count}`
      }
    }
    if(criteria) {
      fullUrl+=`&${criteria}`;
    }
    return this._ApiService.get(fullUrl);
  }


  deleteRecord(url:string , params:string | number) {
    return this._ApiService.delete(url,params);
  }

}
