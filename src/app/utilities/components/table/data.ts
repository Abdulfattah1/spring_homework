import { FormGroup, FormControl } from "@angular/forms";
import { column,columns, elements, settings, actions, action, defaultAction ,Pagination, defaultActionsDisplay} from "./settings";
export class Data {


    
    private data:any[];
    private column:column;
    private privileges:{};
    private columns:columns;
    private actions:actions;
    private settings:settings;
    private elements:elements;
    private filters:FormGroup;
    private selects:FormGroup;
    private totalNumber:number;
    private columnsNames:string[];
    private columnsTitles:string[];
    private pagination:Pagination;
    private defaultActionsArray:string[];
    constructor(settings:settings) {

        this.data = [];
        this.settings = settings;
        this.columns = this.settings.columns;
        this.actions = this.settings.actions;
        this.elements = this.settings.columns.elements;
        this.extractColumnsNames();        
        this.defaultActionsArray = ['add','edit','delete','export','merge'];
    };

    isEnternal() {
        return this.settings.mode == 'enternal'
    }

    isExternal() {
        return this.settings.mode == 'external';
    }

    hasFilters() {
        return this.settings.filters;
    }

    private extractColumnsNames() {
        this.columnsNames = Object.keys(this.elements);
    }

    setData(data:any[]) {
        this.data = data;
    }

    setTotalNumber(count:number) {
        this.totalNumber = count;
    }

    getTotalNumber() {
        return this.data.length;
    }

    createPagination(page?:number,totalSize?:number) {

        let pagination:Pagination = this.settings.pagination || this.getDefaultPagination();
        pagination.page = page;
        if(totalSize) {
           pagination.collectionSize = totalSize;
        } else {
           pagination.collectionSize = this.getTotalNumber();
        }
        this.pagination = pagination;
    }

    setPrivileges(privileges:{}) {
        this.privileges = privileges || {};
    }

    updatePagination(page?:number , collectionSize?:number , pageSize?:number , maxSize?:number , size?:"lg"|"sm") {
        
        this.pagination.page = page || this.pagination.page;
        if(collectionSize!=undefined){
        this.pagination.collectionSize = collectionSize;
        }
        this.pagination.pageSize = pageSize || 10;
        this.pagination.maxSize = maxSize || this.pagination.maxSize;
        this.pagination.size = size || this.pagination.size;
        
    }

    setPagination() {
        if(this.settings.pagination) {
            this.pagination = this.settings.pagination;
        } else {
            this.pagination = this.getDefaultPagination();
        }
        return this.pagination;
    }

    getDefaultPagination() {
        let pagination:Pagination;
        pagination = {
            size:'lg',
            page: 1,
            pageSize: 10,
            maxSize: 5,
            collectionSize:0 
        }
        return pagination;
    }

    getPagination() {
        return this.pagination;
    }

    initFitlers(values:{}) {
        let searchFields:{[propName:string]:string} = {};
        Object.keys(values).forEach(key=>{
            if(values[key]) {
                searchFields[key] = values[key];
            }
          })
          return searchFields;
    }

    createSelect() {
        this.selects = new FormGroup({});
        for (let i = 0; i < this.data.length; i++) {
            let _formControl = this.addFormControl(null);
            let str:string = i.toString();
            this.selects.addControl(str,_formControl);         
        }
    }

    getSelects() {
        return this.selects;
    }

    initQueryParams(values:{}) {
        let queryParams:string = '';
        if(!values) return queryParams;
        Object.keys(values).forEach(key=>{
            if(values[key]) {
                queryParams+=`${key}=${values[key]}&`;
            }
          })
        //   if(queryParams[queryParams.length-1] == '&') {
        //     queryParams = queryParams.slice(0,queryParams.length-1);
        // }
          return queryParams;
    }

    getFilteredData(criteria?:{[propName:string]:string}) {

        let filteredArray:any[];

        if(criteria) {
            filteredArray =this.filter(criteria);   
        } else {
            filteredArray = this.data;
        }
        this.updatePagination(this.pagination.page,filteredArray.length);
        
        filteredArray = this.paginationFunction(filteredArray);

        return filteredArray;
    }

    filter(criteria?:{[propName:string]:string}){  
        
        let pass = true;
        let arr = this.data.filter((item,index)=>{
        for(let key in criteria) {
            let str1:string = item[key];
            let str2:string = criteria[key];
            str1 = str1.toLocaleLowerCase().trim();
            str2 = str2.toLocaleLowerCase().trim();
            pass = str1.includes(str2);
            if(!pass) {
                break;
            }
        }
        
        return pass;
        })
        return arr;
    }

    paginationFunction(filteredArray:any[]) {

        let res:any[] ;
        let start = (this.pagination.page -1) * (this.pagination.pageSize);
        let end = (this.pagination.page) * this.pagination.pageSize;
        res = filteredArray.slice(start,end);
        this.pagination.page = this.pagination.page;
        this.pagination.pageSize = this.pagination.pageSize;
        return res;
    }


    getCustomActions() {
        if(this.actions && this.actions.custom)
        {
            this.actions.custom = this.checkActionPrivileges();
            return this.actions.custom;
        }
        return [];
    }

    getDefaultActionsStatus() {
        let _defaultActions:defaultActionsDisplay = {
            add:false,
            edit:false,
            delete:false,
            export:false,
            view:false,
            merge:false
        };
        if(this.actions && this.actions.default) {
            this.defaultActionsArray.forEach(action=>{
                if(this.actions.default[action]) {
                    this.checkActionPrivilege(this.actions.default[action])
                    ? _defaultActions[action] = true : _defaultActions[action] = false;
                }
            })
        }
        console.log(_defaultActions);
        return _defaultActions;
    }

    checkActionPrivileges():action[] {
        let _custom:action[] = [];
        this.actions.custom.forEach(action=>{
            action.privileges ? this.checkActionPrivilege(action) ? _custom.push(action) : null : _custom.push(action);
        })
        return _custom;
    }

    checkActionPrivilege(action:action | defaultAction):boolean {
        let role_id = localStorage.getItem('role_id');
        let flag:boolean = false;
        if(!action.privileges) return true;
         action.privileges.forEach(privilege=>{
            if(privilege == role_id) {
                flag = true;
            }
        })   
        return flag;
    }


    getColumnsNames() {
        return this.columnsNames;
    }

    getColumnsTitle() {
        this.columnsTitles = [];
        Object.keys(this.elements).forEach(key=>{
            this.columnsTitles.push(this.elements[key]['title']);
        })
        return this.columnsTitles;
    }

    getColumnsClasses() {
        return this.columns.classes;
    }

    getColumnsStyles() {
        return this.columns.styles;
    }

    getColumns() {
        return this.columns.elements;
    }

    getData() {
        return this.data
    }

    getDataSize() {
        return this.data.length;
    }

    isCustomGettingData() {
        if(this.settings.data.custom) {
            return this.settings.data.custom.status;
        }
    }

    extractData(data:any[]) {
        return this.settings.data.custom.getData(data);
    }

    getDataUrl() {
        return this.settings.data.url || '';
    }

    createFilters() {
        this.filters = new FormGroup({});
        this.columnsNames.forEach(name=>{
            this.filters.addControl(name,this.addFormControl(null));
        })
    }

    private addFormControl(value:string | number) {
        let formControl:FormControl;
        formControl = new FormControl(value)
        return formControl;
    }

    getFilters() {
        return this.filters;
    }

}