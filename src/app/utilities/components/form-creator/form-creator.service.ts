import { Injectable } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormCreatorService {

  constructor(private _apiService:ApiService) { }

  getData(url:string , params?:HttpParams) {
    return this._apiService.get(url , params);
  }

  addRecord(url:string,data:{}) {
    return this._apiService.post(url,data);
  }

  updateRecord(url:string,params:string | number,data:{}) {
    return this._apiService.put(url,params,data);
  }

  convertToParams(params:string) {
    let _params:HttpParams;
  }
}
