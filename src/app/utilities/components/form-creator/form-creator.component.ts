import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FacadeService } from '../../services/facade.service';
import { formInterface, field } from './settings/formInterface';
import { formCreatorSettings } from './settings/settings.class';
import { Data } from './settings/data.class';
import { Form } from './settings/form.class';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';
import { displayType } from './settings/displayType.interface';
import { ApiService } from '../../services/api.service';
import { NbToastrService } from '@nebular/theme';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'form-creator',
  templateUrl: './form-creator.component.html',
  styleUrls: ['./form-creator.component.scss']
})
export class FormCreatorComponent implements OnInit {

  @Input() settings:formInterface;

  @Output() addAction:EventEmitter<any> = new EventEmitter();
  @Output() editAction:EventEmitter<any> = new EventEmitter();


  loading:boolean;
  destroy$:Subject<boolean> = new Subject();
  Data:Data;
  Form:Form;
  title:string;
  fields:field[];
  form:FormGroup;
  displayType:displayType;
  settingsClass:formCreatorSettings;
  constructor(
    private api:ApiService,
    private router:Router,
    private activatedRoute:ActivatedRoute,
    private _toastrService:NbToastrService,
    private _facadeService:FacadeService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.title = this.settings.subject;

    this.settingsClass = new formCreatorSettings();
    if(this.settings) {
      this.settingsClass.setSettings(this.settings);
    }

    this.Form = new Form();
    this.fields = this.settingsClass.getFields();

      if(this.settingsClass.isEditingForm()) {
        this.Data = new Data();
        this.getData().then(data=>{
          this.Data.setData(data);
          this.Form.setValues(this.Data.getData());
        })
        .catch(err=>{});
      }

        this.Form.setFields(this.fields);
        this.form = this.Form.createForm();

        this.loading = false;
  }

  getData() {
    return new Promise((resolve,reject)=>{
      let _dataUrl = this.settingsClass.getTableName();
      let _params = this.settingsClass.getParams();
      this._facadeService.getFormData(_dataUrl +'/'+ _params)
      .pipe(takeUntil(this.destroy$))
      .subscribe(data=>{
        resolve(data);
      },err=>{
        reject(false);
      })
    })
  }

  submit() {
    let _values = {...this.form.value};
    let formType:string = this.settingsClass.getFormType();
    let _tableName = this.settingsClass.getTableName();
    if(formType == 'edit') {
      let _params = this.settingsClass.getParams();
      this._facadeService.updateRecord(_tableName,_params,_values)
      .then(res=>{
        this._toastrService.success(res['message'],'success');
        this.router.navigate(['../../'],{relativeTo:this.activatedRoute});
      })
      .catch(err=>{
        this._toastrService.danger(err['message'],'error');
      });
    } else if(formType == 'add') {
      this._facadeService.addRecord(_tableName,_values)
      .then(res=>{
        this._toastrService.success(res['message'],'success');
        this.router.navigate(['../'],{relativeTo:this.activatedRoute});
      })
      .catch(err=>{
        this._toastrService.danger(err['message'],'error');
      });
    }
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
    this.destroy$.unsubscribe();
  }
}
