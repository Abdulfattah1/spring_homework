import { FormGroup, FormControl, Validators } from '@angular/forms';
import { field} from './formInterface';

export class Form {
    form:FormGroup;
    fields:field[];
    constructor() {};
    
    ngOnInit(): void {
        this.form = new FormGroup({});
    }

    setFields(fields) {
        this.fields = fields;
    }

    setValues(values:{}[]){
        this.form.patchValue(values);
    }

    createForm() {
        this.form = this.addFields(this.fields);
        return this.form;
    }

    getForm() {
        return this.form;
    }

    addFields(fields:field[]) {
        let _formGroup:FormGroup = new FormGroup({});
        fields.forEach(field=>{
            let _formControl:FormControl;
            _formControl = this.addField(null,field)
            _formGroup.addControl(field.name,_formControl);
        })
        
        return _formGroup;

    }

    addField(value:string | number,field:field) {
        let _field = new FormControl(value,field.validators);
        // if(field.disabled)
        //     _field.disable();
        return _field;
    }
}