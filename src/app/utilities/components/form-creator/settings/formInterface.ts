import { Validator, Validators } from '@angular/forms';
import { HttpParams } from '@angular/common/http';

export interface formInterface {
    mode:'internal' | 'external',
    type:"add" | "edit";
    disableAll?:boolean;
    tableName:string;
    params?:number | string;
    actions?:actions;
    fields:field[];
    subject:string;
    data?:data
}

export interface actions {
    add?:add;
    edit?:edit;
}

export interface add {
    display:boolean,
    textContent?:string,
    submitingUrl:string,
    attrsToSend?:{}[]
}

export interface edit {

    display:boolean,
    textContent?:string,
    submitingUrl:string,
    attrsToSend?:{}[],
    params:string | number
}

export interface field {
    type?:'number' | 'string' | 'select' | 'email' | 'multi_select';
    name:string;
    title?:string;
    hidden:boolean;
    disabled?:boolean;
    placeholder?:string;
    validators?:Validators;   
}

export interface data {
    url:string,
    params?:string | number
}


// let userForm:formInterface = {
//     mode:'internal',
//     title:'add',
//     actions:{
//         add:{
//             display:true,
//             textContent:'test',
//             submitingUrl:'admin/users'
//         },
//         edit:{
//             display:true,
//             textContent:'edit',
//             submitingUrl:'admin/submit'
//         }
//     },
//     fields:{
//         test:{
//             type:'string',
//             disabled:true,
//             label:'label',
//             placeholder:'name',
//             validators:[Validators.required,Validators.minLength(5)]
//         }
//     }
// }