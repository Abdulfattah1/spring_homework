import { formInterface, field, edit, add } from './formInterface';
import { HttpClient, HttpParams } from '@angular/common/http';

export class formCreatorSettings {

    private settings:formInterface;
    
    constructor() {};

    public setSettings(settings:formInterface) {
        this.settings = settings;
    }
    public getSettings():formInterface {
        return this.settings;
    }

    public getFields():field[] {
        return this.settings.fields;
    }

    public getFieldsName():string[] {
        return Object.keys(this.settings.fields);
    }

    public isExternal() {
        return this.settings.mode == 'external';
    }

    public isEnternal() {
        return this.settings.mode == 'external';
    }

    public isEditingForm() {
        return this.settings.type == 'edit';
    }

    public isAddingForm() {
        return this.settings.type == 'add';
    }
    
    public isEditForm() {
        return this.settings.mode == 'external';
    }

    public getDataUrl() {
        return this.settings.data;
    }    

    public getTableName() {
        return this.settings.tableName;
    }

    public getParams() {
        return this.settings.params;
    }

    public getFormType() {
        return this.settings.type;
    }

    public getEditInfo():edit {
        return this.settings.actions.edit;
    }

    public getAddInfo():add {
        return this.settings.actions.add;
    }
}