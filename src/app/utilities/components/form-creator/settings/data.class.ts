export class Data {

    private data:{}[];
    constructor() {
    };

    setData(data:any) {
        this.data = data;
    }
    
    getData():{}[] {
        return this.data;
    }
}