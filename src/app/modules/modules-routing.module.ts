import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../utilities/guards/auth.guard';

const routes: Routes = [
    {
      path:'',
        canActivate:[AuthGuard],
      loadChildren:()=>import('./management/management.module').then(m => m.ManagementModule)
    },
    {
        canActivate:[AuthGuard],
        path:'management',
        loadChildren:()=>import('./management/management.module').then(m => m.ManagementModule)
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModulesRoutingModule { }
