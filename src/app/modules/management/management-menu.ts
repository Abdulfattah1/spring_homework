import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = 
[
  {
    title: 'Dashboard',
    icon: 'home-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'management',
    icon: 'menu-2-outline',
    children: [
      {
        title: 'users',
        icon:'people-outline',
        data:['1'],
        link: '/modules/management/users',
      }
      //,
      // {
      //   data:['1'],
      //   title: 'roles',
      //   icon:'checkmark-square-2-outline',
      //   link: '/modules/management/roles',
      // },
      // {
      //   data:['1'],
      //   title: 'privileges',
      //   icon:'alert-circle-outline',
      //   link: '/modules/management/privileges',
      // },
      ,{
        data:['1','2','3'],
        title: 'contacts',
        icon:'credit-card-outline',
        link: '/modules/management/contacts',
      },
      {
        data:['1','2','3'],
        title: 'sections',
        icon:'shopping-bag-outline',
        link: '/modules/management/sections',
      },
      {
        data:['1','2'],
        title: 'orders',
        icon:'credit-card-outline',
        link: '/modules/management/orders',
      }
      // ,
      // {
      //   data:['1'],
      //   title: 'log',
      //   icon:'credit-card-outline',
      //   link: '/modules/management/log',
      // },
    ],
  },
];
