import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagementComponent } from './components/management/management.component';
import { UsersComponent } from './components/users/users.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { UsersTableComponent } from './components/users/users-table/users-table.component';
import { PrivilegesComponent } from './components/privileges/privileges.component';
import { PrivilegesTableComponent } from './components/privileges/privileges-table/privileges-table.component';
import { PrivilegeFormComponent } from './components/privileges/privilege-form/privilege-form.component';
import { RolesComponent } from './components/roles/roles.component';
import { RolesTableComponent } from './components/roles/roles-table/roles-table.component';
import { RoleFormComponent } from './components/roles/role-form/role-form.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactsTableComponent } from './components/contacts/contacts-table/contacts-table.component';
import { ContactFormComponent } from './components/contacts/contact-form/contact-form.component';
import { SectionsComponent } from './components/sections/sections.component';
import { OrdersComponent } from './components/orders/orders.component';
import { LogComponent } from './log/log.component';

const routes: Routes = [
  {
    path:"",
    component:ManagementComponent,
    children:[
      {
        path:'users',
        component:UsersComponent,
        data:['1'],
        children:[
          {
            path:'',
            component:UsersTableComponent
          },
          {
            path:'add',
            component:UserFormComponent
          },
          {
            path:'edit/:id',
            component:UserFormComponent
          }
        ]
      },
      {
        path:'privileges',
        data:['1'],
        component:PrivilegesComponent,
        children:[
          {
            path:'',
            component:PrivilegesTableComponent
          },
          {
            path:'add',
            component:PrivilegeFormComponent
          },
          {
            path:'edit/:id',
            component:PrivilegeFormComponent
          }
        ]
      },
      {
        path:'roles',
        data:['1'],
        component:RolesComponent,
        children:[
          {
            path:'',
            component:RolesTableComponent
          },
          {
            path:'add',
            component:RoleFormComponent
          },
          {
            path:'edit/:id',
            component:RoleFormComponent
          }
        ]
      },
      {
        path:'contacts',
        data:['1','2','3'],
        component:ContactsComponent,
        children:[
          {
            path:'',
            component:ContactsTableComponent
          },
          {
            path:'add',
            component:ContactFormComponent
          },
          {
            path:'edit/:id',
            component:ContactFormComponent
          }
        ]
      },
      {
        data:['1','2','3'],
        path:'sections',
        component:SectionsComponent
      },
      {
        data:['1','2','3'],
        path:'orders',
        component:OrdersComponent
      },
      {
        data:['1','2','3'],
        path:'log',
        component:LogComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagementRoutingModule { }
