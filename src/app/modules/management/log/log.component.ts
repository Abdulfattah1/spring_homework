import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../utilities/services/facade.service';
import { settings } from '../../../utilities/components/table/settings';

@Component({
  selector: 'log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit {

  data:any[];
  display:boolean;

  settings:settings = {
    mode:"external",
    filters:true,
    data:{
      url:'log'
    },
    columns: {
      elements:{
        name: {
          title: this._facadeService.translate("name")
        },
        email: {
          title: this._facadeService.translate("email")
        },
        action: {
          title: this._facadeService.translate("section")
        },
        table: {
          title: this._facadeService.translate("table name")
        },
        role: {
          title: this._facadeService.translate("role")
        },
        created_at: {
          title: this._facadeService.translate("created at")
        }
    }
  }
  };
  constructor(
    private _facadeService:FacadeService
  ) { }

  ngOnInit() {
    this._facadeService.get('log')
    .subscribe(data=>{
      this.data = data['data'];
      this.display = true;
    })
  }


}
