import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManagementRoutingModule } from './management-routing.module';
import { ManagementComponent } from './components/management/management.component';
import { UtilitiesModule } from '../../utilities/utilities.module';
import { SharedModule } from '../../shared/shared.module';
import { ThemeModule } from '../../@theme/theme.module';
import { NbMenuModule, NbToastrModule } from '@nebular/theme';
import { UsersComponent } from './components/users/users.component';
import { UserFormComponent } from './components/users/user-form/user-form.component';
import { UsersTableComponent } from './components/users/users-table/users-table.component';
import { PrivilegesComponent } from './components/privileges/privileges.component';
import { PrivilegesTableComponent } from './components/privileges/privileges-table/privileges-table.component';
import { PrivilegeFormComponent } from './components/privileges/privilege-form/privilege-form.component';
import { RolesComponent } from './components/roles/roles.component';
import { RolesTableComponent } from './components/roles/roles-table/roles-table.component';
import { RoleFormComponent } from './components/roles/role-form/role-form.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { ContactsTableComponent } from './components/contacts/contacts-table/contacts-table.component';
import { ContactFormComponent } from './components/contacts/contact-form/contact-form.component';
import { SectionsComponent } from './components/sections/sections.component';
import { MyContactsComponent } from './components/my-contacts/my-contacts.component';
import { OrdersComponent } from './components/orders/orders.component';
import { LogComponent } from './log/log.component';

@NgModule({
  declarations: [
    ManagementComponent,
     UsersComponent, 
     UserFormComponent, 
     UsersTableComponent,
     PrivilegesComponent,
     PrivilegeFormComponent,
     PrivilegesTableComponent,
     RolesComponent,
     RolesTableComponent,
     RoleFormComponent,
     ContactsComponent,
     ContactsTableComponent,
     ContactFormComponent,
     SectionsComponent,
     MyContactsComponent,
     OrdersComponent,
     LogComponent,
    ],
  imports: [
    UtilitiesModule,
    CommonModule,
    SharedModule,
    ThemeModule,
    NbMenuModule,
    NbToastrModule.forRoot(),
    ManagementRoutingModule
  ]
})
export class ManagementModule { }
