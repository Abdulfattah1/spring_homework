import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../../utilities/services/facade.service';
import { Subject } from 'rxjs';
import { settings } from '../../../../utilities/components/table/settings';

@Component({
  selector: 'contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {
  constructor() {};
  ngOnInit(){};
}
