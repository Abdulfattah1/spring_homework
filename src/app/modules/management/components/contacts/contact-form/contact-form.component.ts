import { Component, OnInit } from '@angular/core';
import { formInterface } from '../../../../../utilities/components/form-creator/settings/formInterface';
import { Subject } from 'rxjs';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  id:number;
  settings:formInterface;
  displayTable:boolean;
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private _facadeService:FacadeService,
    private activatedRoute:ActivatedRoute
  ) {
    this.activatedRoute.params
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.id = data['id'];
      this.initSettings();
    })
  }

  ngOnInit() {}

  initSettings() {
    this.id =  this.id && this.id!=0 ? this.id : null;
    this.settings = {
      mode:"external",
      tableName:"contact",
      subject:"contact",
      params:this.id,
      type:this.id && this.id!=0 ? 'edit' : 'add',
      fields:this._facadeService.getFields('contacts')
    }
  this.displayTable = true;
  }
  ngOnDestroy(): void {
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
