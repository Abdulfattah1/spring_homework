import { Component, OnInit } from '@angular/core';
import { settings } from '../../../../../utilities/components/table/settings';
import { Subject } from 'rxjs';
import { FacadeService } from '../../../../../utilities/services/facade.service';

@Component({
  selector: 'contacts-table',
  templateUrl: './contacts-table.component.html',
  styleUrls: ['./contacts-table.component.scss']
})
export class ContactsTableComponent implements OnInit {

  contacts:any;
  displayTable:boolean;
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private _facadeService:FacadeService
  ) { }
  ngOnInit() {
    this._facadeService.getData('showcontact')
    .subscribe(data=>{
      this.contacts = data;
      this.displayTable = true;
    },err=>{})
  }
  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'contact'
    },
    actions:{
      default:{
        add:{
          attrName:'id',
          url:'add'
        },
        edit:{
          attrName:'id',
          url:'users/edit',
          params:'id'
        },
        delete:{
          attrName:'id',
          params:'id',
          url:'contacts',
          type:'confirmation'
        }
      }
      // ,
      // custom:[
      //   {
      //     title:'status',
      //     privileges:[],
      //     icon:'test'
      //   }
      // ]
    },
    columns: {
      elements:{
        first_name: {
          title: this._facadeService.translate("firstName")
        },
        last_name: {
          title: this._facadeService.translate("lastName")
        },
        mobile: {
          title: this._facadeService.translate("mobile"),
          type:"number"
        },
        address: {
          title: this._facadeService.translate("address")
        }
    }
  }
  };
}
