import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { settings } from '../../../../../utilities/components/table/settings';

@Component({
  selector: 'users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

  data:any;
  displayTable:boolean;
  constructor(
    private _facadeService:FacadeService
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData(){
    this._facadeService.getData('users')
    .subscribe(data=>{
      this.data = data;
      this.data =  this.data.map(item=>{
        let _item = {...item};
        _item['section'] = _item['section']!==null ? item['section']['name'] : '';
        return _item;
      })
      this.displayTable = true;
    },err=>{
      this.data = [{
        name:'abd',
        email:'kh',
        section:'1'
      }]
      this.displayTable = true;
    })
  }
  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'users'
    },
    actions:{
      default:{
        add:{
          attrName:'id',
          url:'add'
        },
        edit:{
          attrName:'id',
          url:'users/edit',
          params:'id'
        },
        delete:{
          attrName:'id',
          params:'id',
          url:'users',
          type:'confirmation'
        }
      }
    },

    columns: {
      elements:{
        name: {
          title: this._facadeService.translate("name")
        },
        email: {
          title: this._facadeService.translate("email")
        },
        section: {
          title: this._facadeService.translate("section")
        }
    }
  }
  };

}
