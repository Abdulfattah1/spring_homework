import { Component, OnInit } from '@angular/core';
import { formInterface } from '../../../../../utilities/components/form-creator/settings/formInterface';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { NbSpinnerService, NbToastrService } from '@nebular/theme';
import { TranslationService } from '../../../../../utilities/services/translation.service';
import { AuthServiceService } from '../../../../../auth/auth-service.service';
import { FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { ApiService } from '../../../../../utilities/services/api.service';

@Component({
  selector: 'user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  id:number;

  type:'edit' | 'add';
  sections:any;
  form:FormGroup;
  signUpForm:FormGroup;
  displaying:boolean;
  distroy$:Subject<boolean> = new Subject();
  mediumRegex = new RegExp(
    "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})"
  );
  constructor(
    private router:Router,
    private _toastrService:NbToastrService,
    private _apiService:ApiService,
    private activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params=>{
      this.init(params['id']);
      this.id =  params['id'];
      this.type = params['id'] ?'edit' : 'add';
    });

    this._apiService.get('sections')
    .subscribe(data=>{
      this.sections = data;
    },err=>{})
  }


  init(id:number) {
    if(id) {
      this.getData(id)
      .subscribe(data=>{
        this.createForm(data);
      })
    } else {
      this.createForm();
    }
  }

  getData(id:number) {
    return this._apiService.get('users/' + id);
  }

  createForm(values?:any) {

    this.signUpForm = new FormGroup({
      name: new FormGroup({
        name: new FormControl(null, [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25)
        ])
      }),
      email: new FormControl(
        null,
        [Validators.required, Validators.email]
        // ,
        // this.checkEmail.bind(this)
      ),
      Password: new FormGroup(
        {
          password: new FormControl(null, [
            Validators.required
            ,
            Validators.pattern(this.mediumRegex)
          ]),
          confirmPassword: new FormControl(null, [Validators.required])
        },
        { validators: this.passwordConfirming }
      ),
      section: new FormControl(null, [Validators.required]),
    });
    if(values) {
      this.patchValues(values);
    }

    // if(this.type ==='edit') {
    //   this.signUpForm.get('Password').get('password').disable();
    //   this.signUpForm.get('confirmPassword').get('password').disable();
    // }
    this.displaying = true;
  }

  patchValues(values?:any) {
    let section = {
      id:values['section']['id'],
      name:values['section']['name']
    }
    this.signUpForm.patchValue({
      name: {
        name: values['name']
      },
      email: values['email'],
      section:section['id']
    });
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get("password").value !== c.get("confirmPassword").value) {
      return { invalid: true };
    }
    return null;
  }

  onSubmit() {

    let dataToSend = {
      "name": this.signUpForm.value['name'].name,
      "email": this.signUpForm.value['email'],
      "password": this.signUpForm.value['Password']['password'],
      "confirmPassword": this.signUpForm.value['Password']['confirmPassword'],
      "section_id": this.signUpForm.value['section'],
    }    
    if(this.type == 'add') {
    this._apiService.post('users',dataToSend)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this._toastrService.success(data['message'],'success');
      setTimeout(() => {
        this.router.navigate(['../'],{relativeTo:this.activatedRoute});
      }, 1000);
    },err=>{
      this._toastrService.danger(err['message'],'error');
    })
  } else{
    this._apiService.put('users',this.id,dataToSend)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this._toastrService.success(data['message'],'success');
      setTimeout(() => {
        this.router.navigate(['../../'],{relativeTo:this.activatedRoute});
      }, 1000);
    },err=>{
      this._toastrService.danger(err['message'],'error');
    })
  }
  } 
}
