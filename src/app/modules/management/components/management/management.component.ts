import { Component, OnInit } from '@angular/core';
import { MENU_ITEMS } from '../../management-menu';
import { NbMenuItem, NbSpinnerService } from '@nebular/theme';
import { FacadeService } from '../../../../utilities/services/facade.service';
@Component({
  selector: 'management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss']
})
export class ManagementComponent implements OnInit {


  menu:NbMenuItem[] = MENU_ITEMS;
  constructor(
    private spinner:NbSpinnerService,
    private _facadeService:FacadeService
    ) { }

  ngOnInit() {
    this.spinner.load();

    //the first solution is to use the recurive function but it dosen't work in this project
    //this.menu = this._facadeService.getMenuItems(MENU_ITEMS);

    



    let role_id = localStorage.getItem('role_id');
    if(role_id =='1') {
      this.menu = this.menu1;
    } 
    if(role_id =='2') {
      this.menu = this.menu2;
    } 
    if(role_id =='3') {
      this.menu = this.menu3;
    } 
  }

  menu1 = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/pages/dashboard',
      home: true,
    },
    {
      title: 'management',
      icon: 'menu-2-outline',
      children: [
        {
          title: 'users',
          icon:'people-outline',
          data:['1'],
          link: '/modules/management/users',
        }
        ,{
          data:['1','2','3'],
          title: 'contacts',
          icon:'credit-card-outline',
          link: '/modules/management/contacts',
        },
        {
          data:['1','2','3'],
          title: 'sections',
          icon:'shopping-bag-outline',
          link: '/modules/management/sections',
        },
        {
          data:['1','2'],
          title: 'orders',
          icon:'credit-card-outline',
          link: '/modules/management/orders',
        },
        {
          data:['1'],
          title: 'log',
          icon:'credit-card-outline',
          link: '/modules/management/log',
        },
      ],
    },
  ];

  menu2 = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/pages/dashboard',
      home: true,
    },
    {
      title: 'management',
      icon: 'menu-2-outline',
      children: [

        {
          data:['1','2','3'],
          title: 'contacts',
          icon:'credit-card-outline',
          link: '/modules/management/contacts',
        },
        {
          data:['1','2','3'],
          title: 'sections',
          icon:'shopping-bag-outline',
          link: '/modules/management/sections',
        }
      ],
    },
  ];

  menu3 = [
    {
      title: 'Dashboard',
      icon: 'home-outline',
      link: '/pages/dashboard',
      home: true,
    },
    {
      title: 'management',
      icon: 'menu-2-outline',
      children: [
        {
          data:['1','2','3'],
          title: 'contacts',
          icon:'credit-card-outline',
          link: '/modules/management/contacts',
        },
        {
          data:['1','2','3'],
          title: 'sections',
          icon:'shopping-bag-outline',
          link: '/modules/management/sections',
        }
      ],
    },
  ];
  

}
