import { Component, OnInit } from '@angular/core';
import { settings } from '../../../../utilities/components/table/settings';
import { FacadeService } from '../../../../utilities/services/facade.service';
import { TableService } from '../../../../utilities/components/table/table.service';

@Component({
  selector: 'orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  display:boolean;
  data:any;
  constructor(
    private _facadeService:FacadeService,
    private _tableService:TableService
  ) { }

  ngOnInit() {
    this._facadeService.get('orders')
    .subscribe(data=>{
      this.data = data;
      this.display = true;
    },err=>{})

    this._tableService.customActions$
    .subscribe(data=>{
      if(data.action == 'accepted') {
        this.accept(data);
      }
    if(data.action == 'rejected') {
        this.reject(data);
      }
    })
  }

  accept(data) {
    let id = this.data[data['index']]['id'];
    this._facadeService.get('orders/accept/' + id)
    .subscribe(data=>{
      this.data.splice(data['index'],1);
      this.data = [...this.data];
    })
  }

  reject(data) {
    let id = this.data[data['index']]['id'];
    this._facadeService.get('orders/reject/' + id)
    .subscribe(data=>{
      this.data.splice(data['index'],1);
      this.data = [...this.data];
    })
  }
  
  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'my-contacts'
    },
    actions:{
      default:{
      },
      custom:[
        {
          name:'accepted',
          icon:'checkmark-circle-2-outline',
          title:'accept',
          privileges:["1","2"]
        },
          {
          name:'rejected',
          icon:'close-circle-outline',
          title:'reject',
          privileges:["1","2"]
        }
      ]
    },

    columns: {
      elements:{
        first_name: {
          title: this._facadeService.translate("first name")
        },
        last_name: {
          title: this._facadeService.translate("last name")
        },
        mobile: {
          title: this._facadeService.translate("mobile")
        },
        address: {
          title: this._facadeService.translate("address")
        },
        type: {
          title: this._facadeService.translate("type")
        }
    }
  }
  };

}
