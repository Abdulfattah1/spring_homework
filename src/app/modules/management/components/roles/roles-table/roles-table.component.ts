import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { settings } from '../../../../../utilities/components/table/settings';

@Component({
  selector: 'roles-table',
  templateUrl: './roles-table.component.html',
  styleUrls: ['./roles-table.component.scss']
})
export class RolesTableComponent implements OnInit {

  data = [
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"}
  ]

  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'admin/roles'
    },
    actions:{
      default:{
        add:{
          attrName:'id',
          url:'add'
        },
        edit:{
          attrName:'id',
          url:'roles/edit',
          params:'id'
        },
        delete:{
          attrName:'id',
          params:'id',
          url:'roles/delete',
          type:'confirmation'
        }
      }
    },

    columns: {
      elements:{
        name: {
        title: this._facadeService.translate("name"),
        type:'string'
      }
    }
  }
  };
  constructor(
    private _facadeService:FacadeService
  ) { }

  ngOnInit() {
  }

}
