import { Component, OnInit } from '@angular/core';
import { formInterface } from '../../../../../utilities/components/form-creator/settings/formInterface';
import { Subject } from 'rxjs';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.scss']
})
export class RoleFormComponent implements OnInit {

  id:number;
  settings:formInterface;
  displayTable:boolean;
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private _facadeService:FacadeService,
    private activatedRoute:ActivatedRoute
  ) {
    this.activatedRoute.params
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.id = data['id'];
      this.initSettings();
    })
  }

  ngOnInit() {}

  initSettings() {
    this.id =  this.id && this.id!=0 ? this.id : null;
    this.settings = {
      mode:"external",
      tableName:"roles",
      subject:"roles",
      params:this.id,
      type:this.id && this.id!=0 ? 'edit' : 'add',
      fields:this._facadeService.getFields('roles')
    }
  this.displayTable = true;
  }
  ngOnDestroy(): void {
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
