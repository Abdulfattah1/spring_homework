import { Component, OnInit, ViewChild } from '@angular/core';
import { FacadeService } from '../../../../utilities/services/facade.service';
import { settings } from '../../../../utilities/components/table/settings';
import { TableService } from '../../../../utilities/components/table/table.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss']
})
export class SectionsComponent implements OnInit {


  @ViewChild('mergeReff',{static: false}) mergeReff;

  sections:any;
  mergedContacts:any[];
  displayTabs:boolean;
  mergedObject:{
    first_name:"",
    last_name:"",
    address:"",
    mobile:""
  };
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private _modal:NgbModal,
    private _tableService:TableService,
    private _facadeService:FacadeService,
    private toastrService:NbToastrService
  ) { }

  ngOnInit() {
    this.mergedObject = {
      first_name:'',
      last_name:'',
      mobile:'',
      address:''
    };
    this._facadeService.getData('sections/contacts')
    .subscribe(data=>{
      this.sections
      this.sections = data;

      this.displayTabs = true;
    },err=>{
      // this.sections = [
      //   {
      //     sectionName:'section name',
      //     contacts:[
      //       {
      //         id:1,
      //         first_name:"test111111111",
      //         last_name:"test1",
      //         mobile:'mobile111',
      //         address:'address111'
      //       },
      //       {
      //         id:2,
      //         first_name:"test22222222",
      //         last_name:"test22222222",
      //         mobile:'mobile222',
      //         address:'address22'
      //       },
      //       {
      //         id:3,
      //         first_name:"test",
      //         last_name:"test",
      //         mobile:'mobile',
      //         address:'address'
      //       }
      //     ]
      //   },
      //   {
      //     sectionName:'section name',
      //     contacts:[
      //       {
      //         id:1,
      //         first_name:"test1",
      //         last_name:"test111",
      //         mobile:'mobile11',
      //         address:'address11'
      //       },
      //       {
      //         id:2,
      //         first_name:"test",
      //         last_name:"test",
      //         mobile:'mobile',
      //         address:'address'
      //       },
      //       {
      //         id:3,
      //         first_name:"test",
      //         last_name:"test",
      //         mobile:'mobile',
      //         address:'address'
      //       }
      //     ]
      //   }
      // ]
      this.displayTabs = true;
    })

    this._tableService.customActions$
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      if(data.action == 'merge') {
        this.merge(data);
      }
    })
  }

  addMergedValue(status:boolean,name,value) {
    if(!status && this.mergedObject[name]!=value) {
      this.mergedObject[name] = '';
      return;
    }
    this.mergedObject[name] = value;
  }

  merge(data) {

    let ids:any[] = [];
    let sectionId = data.index;
    let tableIds:number[] = data['ids'];
    this.mergedContacts = [];
    this.mergedObject = {
      first_name:'',
      last_name:'',
      mobile:'',
      address:''
    };
    this.mergedContacts = data['data'];
        this.mergedContacts.forEach(item=>{
      let _id = item['id'];
      ids.push(_id);
    })
    let prop = {
      contact:this.mergedObject
    }
    this._modal.open(this.mergeReff,{size:'lg'}).result
    .then(res=>{
      if(res=='ok') {
        let dataToSend = {
          ids:ids,
          ...prop.contact
        }
        this._facadeService.post('contact/merge',dataToSend)
        .subscribe(data=>{
          for(let i = tableIds.length-1 ; i >=0;i--) {
            console.log(tableIds[i]);
            this.sections[sectionId]['contacts'].splice(tableIds[i],1);
          }
          this.sections[sectionId]['contacts'].push(data['contact']);
          this.sections[sectionId]['contacts'] = [...this.sections[sectionId]['contacts']];
          
          this.toastrService.success(data['meg'],'success');

        },err=>{
          this.toastrService.danger('please refresh the page','error');
        })
      }
    })
    .catch(err=>{})
  }
  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'api/users'
    },
    actions:{
      default:{
        merge:{
          attrName:'merge',
          url:'merge'
        }
      }
    },
    columns: {
      elements:{
        first_name: {
          title: this._facadeService.translate("first name")
        },
        last_name: {
          title: this._facadeService.translate("last name")
        },
        mobile: {
          title: this._facadeService.translate("mobile")
        },
        address: {
          title: this._facadeService.translate("address")
        }
    }
  }
  };

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
