import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../utilities/services/api.service';
import { settings } from '../../../../utilities/components/table/settings';
import { FacadeService } from '../../../../utilities/services/facade.service';

@Component({
  selector: 'my-contacts',
  templateUrl: './my-contacts.component.html',
  styleUrls: ['./my-contacts.component.scss']
})
export class MyContactsComponent implements OnInit {

  display:boolean;
  data:any[];
  constructor(
    private _facadeService:FacadeService,
    private apiSevice:ApiService
  ) { }

  ngOnInit() {
    this._facadeService.get('contacts')
    .subscribe(data=>{
      this.data = data['data'];
      this.display = true;
    },err=>{})
  }

  settings:settings = {
    mode:"external",
    filters:true,
    data:{
      url:'contacts'
    },
    actions:{
      default:{
        add:{
          attrName:'id',
          url:'add'
        },
        edit:{
          attrName:'id',
          url:'users/edit',
          params:'id'
        },
        delete:{
          attrName:'id',
          params:'id',
          url:'contacts',
          type:'confirmation'
        }
      }
    },

    columns: {
      elements:{
        first_name: {
          title: this._facadeService.translate("first name")
        },
        last_name: {
          title: this._facadeService.translate("last name")
        },
        mobile: {
          title: this._facadeService.translate("mobile")
        },
        address: {
          title: this._facadeService.translate("address")
        }
    }
  }
  };

}
