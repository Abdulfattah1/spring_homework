import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { settings } from '../../../../../utilities/components/table/settings';

@Component({
  selector: 'privileges-table',
  templateUrl: './privileges-table.component.html',
  styleUrls: ['./privileges-table.component.scss']
})
export class PrivilegesTableComponent implements OnInit {

  data = [
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"},
    {name:"abd"}
  ]

  settings:settings = {
    mode:"enternal",
    filters:true,
    data:{
      url:'admin/privileges'
    },
    actions:{
      default:{
        add:{
          attrName:'id',
          url:'add'
        },
        edit:{
          attrName:'id',
          url:'users/edit',
          params:'id'
        },
        delete:{
          attrName:'id',
          params:'id',
          url:'users/delete',
          type:'confirmation'
        }
      }
    },

    columns: {
      elements:{
        name: {
        title: this._facadeService.translate("name"),
        type:'string'
      }
    }
  }
  };
  constructor(
    private _facadeService:FacadeService
  ) { }

  ngOnInit() {
  }
}
