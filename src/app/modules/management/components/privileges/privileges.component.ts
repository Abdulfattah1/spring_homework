import { Component, OnInit } from '@angular/core';
import { FacadeService } from '../../../../utilities/services/facade.service';
import { settings } from '../../../../utilities/components/table/settings';

@Component({
  selector: 'privileges',
  templateUrl: './privileges.component.html',
  styleUrls: ['./privileges.component.scss']
})
export class PrivilegesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
