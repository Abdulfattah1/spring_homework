import { Component, OnInit } from '@angular/core';
import { actions, data, formInterface } from '../../../../../utilities/components/form-creator/settings/formInterface';
import { Subject } from 'rxjs';
import { FacadeService } from '../../../../../utilities/services/facade.service';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'privilege-form',
  templateUrl: './privilege-form.component.html',
  styleUrls: ['./privilege-form.component.scss']
})
export class PrivilegeFormComponent implements OnInit {

  id:number;
  settings:formInterface;
  displayTable:boolean;
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private _facadeService:FacadeService,
    private activatedRoute:ActivatedRoute
  ) {
    this.activatedRoute.params
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this.id = data['id'];
      this.initSettings();
    })
  }

  ngOnInit() {}

  initSettings() {
    this.id =  this.id && this.id!=0 ? this.id : null;
    this.settings = {
      mode:"external",
      tableName:"privileges",
      subject:"privileges",
      params:this.id,
      type:this.id && this.id!=0 ? 'edit' : 'add',
      fields:this._facadeService.getFields('privileges')
    }
  this.displayTable = true;
  }
  ngOnDestroy(): void {
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
