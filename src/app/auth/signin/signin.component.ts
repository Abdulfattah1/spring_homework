import { Component, OnInit } from '@angular/core';
import { NbSpinnerService, NbToastrService } from '@nebular/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../auth-service.service';
import { tap, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { TranslationService } from '../../utilities/services/translation.service';

@Component({
  selector: 'signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  form:FormGroup;
  distroy$:Subject<boolean> = new Subject();
  mediumRegex = new RegExp(
    "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})"
  );
  constructor(
    private router:Router,
    private spinner:NbSpinnerService,
    private toastr:NbToastrService,
    private _translationService:TranslationService,
    private authService:AuthServiceService
  ) { }

  ngOnInit() {
    this.spinner.load();
    this.form = new FormGroup({
      email:new FormControl(null,[Validators.required,Validators.minLength(3)]),
      password:new FormControl(null,[
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(this.mediumRegex)])
    })
  }


  Submit() {
    this.authService.signin(this.form.value)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      localStorage.setItem('token',data['token']);
      this.authService.getUserId()
      .subscribe(data=>{
        localStorage.setItem('role_id',data['role_id']);
      })
      
      this.toastr.success(data['message'],'success');
      setTimeout(() => {
        this.router.navigate(['pages','dashboard']);
      }, 1000);
    },err=>{
      // localStorage.setItem('token','test');
      // localStorage.setItem('role_id','1');
      // this.router.navigate(['pages','dashboard']);
      this.toastr.danger(err['message'],'error');
    })
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
