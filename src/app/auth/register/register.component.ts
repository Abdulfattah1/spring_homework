import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { AuthServiceService } from '../auth-service.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {


  mediumRegex = new RegExp(
    "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})"
  );
  signUpForm:FormGroup;
  distroy$:Subject<boolean> = new Subject();
  constructor(
    private router:Router,
    private _toastrService:NbToastrService,
    private _authService:AuthServiceService
  ) {}


  ngOnInit() {
    this.init();
  }


  init() {
    this.signUpForm = new FormGroup({
      name: new FormGroup({
        firstName: new FormControl(null, [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25)
        ]),
        lastName: new FormControl(null, [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(25)
        ])
      }),
      email: new FormControl(
        null,
        [Validators.required, Validators.email]
        // ,
        // this.checkEmail.bind(this)
      ),
      Password: new FormGroup(
        {
          password: new FormControl(null, [
            Validators.required
            ,
            Validators.pattern(this.mediumRegex)
          ]),
          confirmPassword: new FormControl(null, [Validators.required])
        },
        { validators: this.passwordConfirming }
      ),
      role: new FormControl(null, [Validators.required])
    });

    this.signUpForm.patchValue({
      name: {
        firstName: "abdulfattah",
        lastName: "khudari"
      },
      email: "abdulfattah.khudari@gmail.com",
      Password: {
        password: "abdulfattah0952432706",
        confirmPassword: "abdulfattah0952432706"
      },
      Birthday: "5/5/1995",
      mobileNumber: "0952432706",
      gender: "male"
    });
  }

  passwordConfirming(c: AbstractControl): { invalid: boolean } {
    if (c.get("password").value !== c.get("confirmPassword").value) {
      return { invalid: true };
    }
    return null;
  }

  onSubmit() {
    let dataToSend = {...this.signUpForm.value};

    this._authService.register(dataToSend)
    .pipe(takeUntil(this.distroy$))
    .subscribe(data=>{
      this._toastrService.success(data['message'],'success');
    },err=>{
      this._toastrService.danger(err['message'],'error');
      setTimeout(() => {
        this.router.navigate(['auth','login']);
      }, 1000);
    })
  }

  // checkEmail(control: FormControl) {
  //   const promise = new Promise((reslove, rejcet) => {
  //     let sub = this._facadeService.checkEmail(control.value).subscribe(response => {
  //       const RES = response.json();
  //       if (!RES.success) {
  //         reslove({ emailIsUsed: true });
  //       } else {
  //         reslove(null);
  //       }
  //     });
  //   });

  //   return promise;
  // }

  ngOnDestroy(): void {
    this.distroy$.next(true);
    this.distroy$.complete();
    this.distroy$.unsubscribe();
  }
}
