import { Injectable } from '@angular/core';
import { ApiService } from '../utilities/services/api.service';
import { JwtHelperService } from '@auth0/angular-jwt';

const jwtHelper = new JwtHelperService();
@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(
    private _apiService:ApiService
  ) { }

  signin(userData:{userName:string,password:number}) {
    return this._apiService
    .post('login',userData);
  }
  register(userData:{name:string,email:string,password:string,confirmPassword:string,section_id:number}) {
    return this._apiService.post('register',userData);
  }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    const role_id = localStorage.getItem('role_id');
    //return true;
    // if(token)
      return true;
      
    return false;
  }

  getUserId() {
    return this._apiService.get('getuserId');
  }
  logOut() {
    return this._apiService.get('logout');
  }

}
