import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { RegisterComponent } from './register/register.component';
import { AuthRoutingModule } from './auth.routing.module';
import { NbMenuModule, NbButtonModule, NbInputModule } from '@nebular/theme';
import { ThemeModule } from '../@theme/theme.module';
import { SharedModule } from '../shared/shared.module';
import { NbAuthModule } from '@nebular/auth';
import { UtilitiesModule } from '../utilities/utilities.module';

@NgModule({
  declarations: [SigninComponent, RegisterComponent],
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    SharedModule,
    NbAuthModule,
    NbInputModule,
    NbButtonModule,
    AuthRoutingModule,
    UtilitiesModule
  ]
})
export class AuthModule { }
